function getBMWAudiCars(inventory) {

    let car = []
    if (!Array.isArray(inventory) || inventory.length == 0) {
        return car;
    }


    let bmwAndAudiCars = inventory.filter((car) => {
        if (car.car_make === "BMW" || car.car_make === "Audi") {
            return car;
        }
    });
    return bmwAndAudiCars;
}


module.exports = getBMWAudiCars;