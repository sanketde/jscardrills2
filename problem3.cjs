function getSortedCarListByModel(inventory) {

  let car = []
  if (!Array.isArray(inventory) || inventory.length == 0) {
    return car;
  }
  const carModelList = inventory.map(function (car) {
    return car.car_model;
  });
  carModelList.sort()
  return carModelList;
}


module.exports = getSortedCarListByModel;