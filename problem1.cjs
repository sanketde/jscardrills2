function getCarById(inventory, id) {
  let car = []
  if (!Array.isArray(inventory) || inventory.length == 0) {
    return car;
  }
  if (typeof id !== 'number') {
    return car;
  }

  const filteredArray = inventory.filter(function (element) {
    return element.id == id;
  });

  return filteredArray[0];

}


module.exports = getCarById;
