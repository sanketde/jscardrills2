function getLastCar(inventory) {
  let car = []
  if (!Array.isArray(inventory) || inventory.length == 0) {
    return car;
  }

  const lastCar = inventory.filter(function (element) {
    return element.id == inventory.length;
  });

  return lastCar[0];
}


module.exports = getLastCar;
