let getAllCarYears = require('./problem4.cjs');

function getOldCarCount(inventory) {
    let car = []
    if (!Array.isArray(inventory) || inventory.length == 0) {
        return car;
    }

    let count = 0;
    let carYears = getAllCarYears(inventory);
    let oldCars = carYears.filter((year) => {
        if (year < 2000) {
            return year;
        }
    });

    return oldCars.length;
}


module.exports = getOldCarCount;