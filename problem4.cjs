function getAllCarYears(inventory) {
    let car = []
    if (!Array.isArray(inventory) || inventory.length == 0) {
        return car;
    }

    let carYears = inventory.map((car) => car.car_year);
    return carYears;
}


module.exports = getAllCarYears;